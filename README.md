## 基于 Spring Webflux 的输入文字并返回统计各词组出现次数
1. given 一个页面, when do nothing, then 可以看到一个文本输入框和一个"确认"按钮
2. given 一个页面, when 点击"确认"按钮时, then 可以弹出一个内容为空的弹窗
3. given 一个POST API(`/user/result`), when post该请求时, then 返回一个"successful"的message
4. given 一个POST API(`/user/result`), when post该请求时, then 返回词组及频率
5. given 一个页面, when 拿到出现的词组及频率, then 在页面上显示词组及频率

## 登陆注册
1. given 一个POST API(`/user/register`),when post该请求时, then 返回"register successful"的消息。
2. given 一个GET API(`/user/{id}/login`),when 已注册的用户get该请求时, then 返回用户信息。
3. given 一个GET API(`/user/{id}/login`),when 未注册的用户get该请求时, then 返回"don`t exist user,please to register"。
4. given 一个GET API(`/user/{id}/all`),when 包含有用户时, then 返回所有的用户信息。
5. given 一个PUT API(`/user/{id}/update`), when 该用户存在时, then 返回用户修改后的信息。
6. given 一个PUT API(`/user/{id}/update`), when 该用户不存在时, then 返回"user not exist,please to register"。
7. given 一个DELETE API(`/user/{id}/delete`), when id存在时，then 删除成功后返回"delete successful"。
8. given 一个DELETE API(`/user/{id}/delete`), when id不存在时，then 返回"user is not exist"。