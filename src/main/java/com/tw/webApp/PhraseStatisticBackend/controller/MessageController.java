package com.tw.webApp.PhraseStatisticBackend.controller;

import com.tw.webApp.PhraseStatisticBackend.domain.Message;
import com.tw.webApp.PhraseStatisticBackend.domain.Words;
import com.tw.webApp.PhraseStatisticBackend.service.MessageService;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@RestController
public class MessageController {

    private MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/result")
    public Mono<List<Words>> returnResult(@Valid @RequestBody Message message) {

        String input = message.getMessage();

        return Mono.just(messageService.getResult(input));
    }

    @PostMapping("/upload")
    public Mono<List<Words>> upload(@RequestPart("file") FilePart filePart) throws IOException {
        Path tempFile = Files.createTempFile("test", filePart.filename());

        //写入到文件
        filePart.transferTo(tempFile.toFile());

        File inputFile = Mono.just(tempFile.toFile()).toProcessor().block();

        String inputString = getStringFromFile(inputFile);

        return Mono.just(messageService.getResult(inputString));
    }

    private String getStringFromFile(File inputFile) {

        int len = 0;
        StringBuilder str = new StringBuilder("");
        try {
            FileInputStream is = new FileInputStream(inputFile);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader in = new BufferedReader(isr);
            String line = "";
            while ((line = in.readLine()) != null) {
                if (len != 0) {
                    str.append("\r\n").append(line);
                } else {
                    str.append(line);
                }
                len++;
            }
            in.close();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

}
