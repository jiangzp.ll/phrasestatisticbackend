package com.tw.webApp.PhraseStatisticBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhraseStatisticBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhraseStatisticBackendApplication.class, args);
	}

}
