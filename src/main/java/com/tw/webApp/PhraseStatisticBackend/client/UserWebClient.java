package com.tw.webApp.PhraseStatisticBackend.client;

import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class UserWebClient {
    private WebClient client = WebClient.create("http://localhost:8080");

    private Mono<ClientResponse> result = client.post()
            .uri("/result")
            .accept(MediaType.APPLICATION_JSON)
            .exchange();

    private Mono<ClientResponse> register = client.post()
            .uri("/user/register")
            .accept(MediaType.APPLICATION_JSON)
            .exchange();

    private Mono<ClientResponse> login = client.get()
            .uri("/user/login/{id}")
            .accept(MediaType.APPLICATION_JSON)
            .exchange();

    private Mono<ClientResponse> getAll = client.get()
            .uri("/user/all")
            .accept(MediaType.APPLICATION_JSON)
            .exchange();

    private Mono<ClientResponse> deleteUser = client.delete()
            .uri("/user/delete/{id}")
            .accept(MediaType.APPLICATION_JSON)
            .exchange();
}
