package com.tw.webApp.PhraseStatisticBackend.config;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@ConfigurationProperties(prefix = "user-cache")
@Getter
@Setter
@Slf4j
public class CacheConfiguration {

    private boolean enabled;
    private boolean localCacheEnabled;
    private long localCacheExpiry;

    @PostConstruct
    public void log() {
        log.debug("Caching status: {}", enabled);
    }

}
