package com.tw.webApp.PhraseStatisticBackend.config;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.bucket.BucketManager;
import com.couchbase.client.java.query.util.IndexInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.core.CouchbaseTemplate;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@Slf4j
public class MyCouchbaseConfig {

    private final CouchbaseTemplate couchbaseTemplate;

    @Autowired
    public MyCouchbaseConfig(CouchbaseTemplate couchbaseTemplate) {
        this.couchbaseTemplate = couchbaseTemplate;
        initQueryIndex();
    }

    private void initQueryIndex() {
        Bucket bucket = couchbaseTemplate.getCouchbaseBucket();
        BucketManager bucketManager = bucket.bucketManager();

        List<IndexInfo> indexInfos = bucketManager.listN1qlIndexes();
        List<String> existIndexes = indexInfos.stream().map(
                IndexInfo::name
        ).collect(Collectors.toList());
        log.debug("Exist indexes {}", existIndexes);

        // create primary index
        bucketManager.createN1qlPrimaryIndex(true, false);

        // create class index
        bucketManager.createN1qlIndex("phrase-statistic-class",
                Collections.singletonList("_class"),
                null,
                true,
                false);
    }
}
