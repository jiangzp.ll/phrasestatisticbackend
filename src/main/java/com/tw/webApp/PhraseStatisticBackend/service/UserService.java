package com.tw.webApp.PhraseStatisticBackend.service;

import com.couchbase.client.java.error.DocumentAlreadyExistsException;
import com.tw.webApp.PhraseStatisticBackend.cache.LocalCacheManager;
import com.tw.webApp.PhraseStatisticBackend.domain.User;
import com.tw.webApp.PhraseStatisticBackend.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class UserService {

    private UserRepository userRepository;

    private LocalCacheManager localCacheManager;

    public UserService(UserRepository userRepository, LocalCacheManager localCacheManager) {
        this.userRepository = userRepository;
        this.localCacheManager = localCacheManager;
    }

    //保存user
    public Mono<User> saveUser(User user) {
        return userRepository.save(user);
    }

    //根据ID查找user
    public Mono<User> getUserById(String userId) {
        return userRepository.findById(userId);
    }

    //从缓存中获得所有的user
    public Flux<User> getAllUsersWithoutCache() {
        log.info("get all users with cache");
        return localCacheManager.cacheAllUsers(this::getAllUsers);
    }
    //从UserRepository中获取所有user
    public Flux<User> getAllUsers() {
        log.info("get all user with repo");
        return userRepository.findAll().cache();
    }

    //修改user信息
    public Mono<User> updateUser(String userId, User user) {
        return existsById(userId)
                .flatMap(exists -> exists
                        ? userRepository.save(new User(userId, user.getName(),user.getPassword()))
                        : Mono.error(new DocumentAlreadyExistsException("user not exist,please to register")));
    }

    //根据ID删除user
    public Mono<Void> deleteUserById(String userId) {
        return getUserById(userId).flatMap(user -> userRepository.deleteById(userId));
    }

    //判断用户ID是否存在
    public Mono<Boolean> existsById(String userId) {
        return userRepository.existsById(userId);
    }
}
