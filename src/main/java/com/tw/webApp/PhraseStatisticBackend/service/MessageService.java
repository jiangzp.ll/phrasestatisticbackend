package com.tw.webApp.PhraseStatisticBackend.service;

import com.tw.webApp.PhraseStatisticBackend.domain.Words;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class MessageService {

    private static final String PUNCTUATION = "[\\p{P}\\p{S}\\p{Z}]+";
    private static final String CHINESE = "[\u4e00-\u9fa5]";

    public List<Words> getResult(String input) {

        List<Words> resultList = new ArrayList<>();
        Map<String, Integer> messageMap = new HashMap<>();

        String[] inputHandle = judgmentLanguage(input);
        return getResultList(inputHandle, messageMap, resultList);
    }

    private List<Words> getResultList(String[] inputArray, Map<String, Integer> map, List<Words> resultList) {

        Arrays.stream(inputArray).forEach(str -> {
            if (!map.containsKey(str)) {
                map.put(str, 1);
            } else {
                Integer iCount = map.get(str);
                iCount++;
                map.put(str, iCount);
            }
        });

        for (Map.Entry<String, Integer> result : map.entrySet()) {
            Words words = new Words(result.getKey(), result.getValue());
            resultList.add(words);
        }

        return resultList;
    }

    private String[] judgmentLanguage(String input) {
        String[] inputArray;
        Pattern pattern = Pattern.compile(CHINESE);
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            inputArray = input.replaceAll(PUNCTUATION, "").split("");
        } else {
            inputArray = input.replaceAll(PUNCTUATION, " ").split(" ");
        }
        return inputArray;
    }

}
