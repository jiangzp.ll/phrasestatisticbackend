package com.tw.webApp.PhraseStatisticBackend.repository;

import com.tw.webApp.PhraseStatisticBackend.domain.User;
import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository;
import reactor.core.publisher.Flux;

public interface UserRepository extends ReactiveCouchbaseRepository<User, String> {

    @Query(value = "#{#n1ql.selectEntity} WHERE #{#n1ql.filter}")
    Flux<User> findAll();
}
