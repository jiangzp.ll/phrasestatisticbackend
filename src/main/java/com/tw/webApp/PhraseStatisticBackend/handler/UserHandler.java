package com.tw.webApp.PhraseStatisticBackend.handler;

import com.tw.webApp.PhraseStatisticBackend.domain.Message;
import com.tw.webApp.PhraseStatisticBackend.domain.User;
import com.tw.webApp.PhraseStatisticBackend.service.MessageService;
import com.tw.webApp.PhraseStatisticBackend.service.UserService;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;


@Component
public class UserHandler {

    private MessageService messageService;

    private UserService userService;

    public UserHandler(MessageService messageService, UserService userService) {
        this.messageService = messageService;
        this.userService = userService;
    }


    //统计用户输入字符串中每个词语出现的频率
    public Mono<ServerResponse> createMessage(ServerRequest request) {

        return request.bodyToMono(Message.class)
                .map(Message::getMessage)
                .flatMap(i -> Mono.just(this.messageService.getResult(i)))
                .flatMap(l -> ServerResponse.ok().body(BodyInserters.fromValue(l)));
    }


    //登陆注册


    //注册(add user)
    public Mono<ServerResponse> addUser(ServerRequest request) {

        return request.bodyToMono(User.class)
                .flatMap(user -> userService.saveUser(user))
                .then(ServerResponse.ok().body(BodyInserters.fromValue("register successful")));

    }

    //登陆(findUserById)
    public Mono<ServerResponse> findUserById(ServerRequest request) {
        String userId = request.pathVariable("id");

        return userService.getUserById(userId)
                .flatMap(user -> ServerResponse.ok().body(BodyInserters.fromValue("login successful")))
                .switchIfEmpty(ServerResponse.badRequest().body(BodyInserters.fromValue("login fail,please to register")));
    }

    //获得所有用户(get all user)
    public Mono<ServerResponse> getAllUser(ServerRequest request) {

        return ServerResponse.ok().body(userService.getAllUsersWithoutCache(), User.class);
    }

    //修改用户信息(update user info)
    public Mono<ServerResponse> updateUserInfoById(ServerRequest request) {

        return request.bodyToMono(User.class)
                .flatMap(user -> userService.existsById(user.getId())
                        .flatMap(exists -> exists ?
                                ServerResponse.ok().body(userService.updateUser(user.getId(),user), User.class) :
                                ServerResponse.badRequest().body(BodyInserters.fromValue("user not exist,please to register"))));
    }

    //删除用户(delete user)
    public Mono<ServerResponse> deleteUserById(ServerRequest request) {
        String userId = request.pathVariable("id");

        return userService.getUserById(userId)
                .flatMap(user -> userService.deleteUserById(user.getId()).then(ServerResponse.ok().body(BodyInserters.fromValue("delete successful"))))
                .switchIfEmpty(ServerResponse.badRequest().body(BodyInserters.fromValue("user id is not exist")));
    }

}
