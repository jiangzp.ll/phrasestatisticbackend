package com.tw.webApp.PhraseStatisticBackend.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tw.webApp.PhraseStatisticBackend.config.CacheConfiguration;
import com.tw.webApp.PhraseStatisticBackend.domain.User;
import org.springframework.stereotype.Component;
import reactor.cache.CacheFlux;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@Component
public class LocalCacheManager {

    private final CacheConfiguration cacheConfiguration;
    private static final String GET_ALL_USER = "getAllUser";

    private Cache<String, List> localCache;

    public LocalCacheManager(CacheConfiguration cacheConfiguration) {
        this.cacheConfiguration = cacheConfiguration;
        localCache = Caffeine.newBuilder()
                .expireAfterWrite(cacheConfiguration.getLocalCacheExpiry(), TimeUnit.SECONDS)
                .maximumSize(100 * 10000)
                .build();
    }

    public Flux<User> cacheAllUsers(Supplier<Flux<User>> supplier) {
        if (!cacheConfiguration.isLocalCacheEnabled()) {
            return supplier.get();
        }
        return CacheFlux
                .lookup(localCache.asMap(), GET_ALL_USER, User.class)
                .onCacheMissResume(supplier);
    }

}
