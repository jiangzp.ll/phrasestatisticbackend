package com.tw.webApp.PhraseStatisticBackend.exception;

import com.tw.webApp.PhraseStatisticBackend.domain.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorCode handlerNullPointerError(NullPointerException ex) {
        return new ErrorCode(10000, "request not empty");
    }
}

