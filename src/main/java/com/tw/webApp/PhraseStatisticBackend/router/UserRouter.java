package com.tw.webApp.PhraseStatisticBackend.router;

import com.tw.webApp.PhraseStatisticBackend.handler.UserHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class UserRouter {

    @Bean
    public RouterFunction<ServerResponse> router(UserHandler messageHandler) {
        return route(POST("/result"), messageHandler::createMessage)
                .andRoute(POST("/user/register"), messageHandler::addUser)
                .andRoute(GET("/user/login/{id}"), messageHandler::findUserById)
                .andRoute(GET("/user/all"), messageHandler::getAllUser)
                .andRoute(POST("/user/{id}/update"), messageHandler::updateUserInfoById)
                .andRoute(POST("/user/{id}/delete"), messageHandler::deleteUserById);
    }

}
