package com.tw.webApp.PhraseStatisticBackend.domain;

import lombok.*;
import lombok.experimental.Tolerate;

@Data
@Builder
@Getter
@Setter
@AllArgsConstructor
public class Words {

    private String name;

    private Integer count;

    @Tolerate
    public Words() {

    }
}
