package com.tw.webApp.PhraseStatisticBackend.domain;

import javax.validation.constraints.NotBlank;

public class Message {

    @NotBlank
    private String message;

    public Message() {
    }

    public Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
