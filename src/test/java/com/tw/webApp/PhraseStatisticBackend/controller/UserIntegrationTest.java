package com.tw.webApp.PhraseStatisticBackend.controller;

import com.tw.webApp.PhraseStatisticBackend.domain.Message;
import com.tw.webApp.PhraseStatisticBackend.domain.User;
import com.tw.webApp.PhraseStatisticBackend.domain.Words;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserIntegrationTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    protected CouchbaseTemplate couchbaseTemplate;

    private static final String USER_ID = "user-1";

    @Test
    void should_return_200_when_message_is_greek() {
        Message message = new Message("για,αγώνας.ορίστηκε/για< το >αγώνας?ορίστηκε  για.");
        webTestClient.post().uri("/result")
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromPublisher(Mono.just(message), Message.class))
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(Words.class).getResponseBody()
                .map(w -> w.getName() + ":" + w.getCount())
                .subscribe(System.out::println);
    }

    @Test
    void should_return_200_when_message_is_chinese() {
        Message message = new Message("I,feel.strongly?that[I {can|make;it.");
        webTestClient.post().uri("/result")
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromPublisher(Mono.just(message), Message.class))
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(Words.class).getResponseBody()
                .map(w -> w.getName() + ":" + w.getCount())
                .subscribe(System.out::println);
    }

    @Test
    void should_return_200_when_message_is_chinese_and_english() {
        Message message = new Message("哈    }哈《嘿，嘿+");
        webTestClient.post().uri("/result")
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromPublisher(Mono.just(message), Message.class))
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(Words.class).getResponseBody()
                .map(w -> w.getName() + ":" + w.getCount())
                .subscribe(System.out::println);
    }

    @Test
    void should_return_200_when_register() {
        User user = User.builder().id(USER_ID).name("tom").password("pass").build();

        webTestClient
                .post().uri("/user/register")
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(user), User.class)
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(String.class).getResponseBody()
                .subscribe(System.out::println);
    }

    @Test
    void should_return_200_when_login() {

        webTestClient.get().uri("/user/login/" + USER_ID)
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(String.class).getResponseBody()
                .subscribe(System.out::println);

    }

    @Test
    void should_return_400_if_user_is_not_exist_when_login() {
        webTestClient.get().uri("/user/login/111")
                .exchange().expectStatus()
                .isBadRequest()
                .returnResult(String.class).getResponseBody()
                .subscribe(System.out::println);
    }

    @Test
    void should_return_200_when_get_all_user() {
        List<User> users = webTestClient.get().uri("/user/all")
                .exchange().expectStatus()
                .isOk()
                .expectBodyList(User.class).returnResult().getResponseBody();
        assert users != null;
        assertEquals(1,users.size());
    }

    @Test
    void should_return_200_when_update_exist_user() {

        User newUser = User.builder().id(USER_ID).name("kavin").password("pass").build();
        User user = User.builder().id(USER_ID).name("tom").password("pass").build();
        prepareUser(user);

        Flux<User> userFlux = webTestClient.post().uri("/user/" + USER_ID + "/update")
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(newUser), User.class)
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(User.class).getResponseBody()
                .doOnSubscribe(System.out::println);
        assertNotNull(userFlux);
    }

    @Test
    void should_return_400_when_update_not_exist_user() {

        User newUser = User.builder().id("abc").name("kavin").password("pass").build();
        User user = User.builder().id(USER_ID).name("tom").password("pass").build();
        prepareUser(user);

        webTestClient.post().uri("/user/abc/update")
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(newUser), User.class)
                .exchange()
                .expectStatus()
                .isBadRequest()
                .returnResult(String.class).getResponseBody()
                .subscribe(System.out::println);
    }

    @Test
    void should_return_200_when_delete_user_by_id() {
        webTestClient.post().uri("/user/" + USER_ID + "/delete")
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(String.class).getResponseBody()
                .subscribe(System.out::println);
    }

    @Test
    void should_return_400_when_delete_a_not_exist_user_id() {
        webTestClient.post().uri("/user/111/delete")
                .exchange()
                .expectStatus()
                .isBadRequest()
                .returnResult(String.class).getResponseBody()
                .subscribe(System.out::println);
    }

    private void prepareUser(User user) {
        couchbaseTemplate.save(user);
    }
}