package com.tw.webApp.PhraseStatisticBackend.service;

import com.tw.webApp.PhraseStatisticBackend.domain.Message;
import com.tw.webApp.PhraseStatisticBackend.domain.Words;
import com.tw.webApp.PhraseStatisticBackend.service.MessageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MessageServiceTest {

    @Autowired
    private MessageService messageService;

    @Test
    void should_check_get_result_method_in_message_service() {

        List<Words> result = messageService.getResult("hello,world.world!");
        assertEquals(2, result.size());
        assertEquals("world", result.get(0).getName());
        assertEquals(2, result.get(0).getCount());
        assertEquals("hello", result.get(1).getName());
        assertEquals(1, result.get(1).getCount());
    }

    @Test
    void should_check_judgment_language_method_in_message_service() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Method judgmentLanguage = messageService.getClass().getDeclaredMethod("judgmentLanguage", String.class);
        //要访问私有方法必须将accessible设置为true，否则抛java.lang.IllegalAccessException
        judgmentLanguage.setAccessible(true);
        String[] result = (String[])judgmentLanguage.invoke(messageService, "哈,嘿");
        assertNotNull(result);
        assertEquals("哈", result[0]);
        assertEquals("嘿", result[1]);
    }

    @Test
    void should_deal_with_input_to_list_method_in_message_service() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method getResultList = messageService.getClass().getDeclaredMethod("getResultList", String[].class, Map.class,List.class);
        getResultList.setAccessible(true);
        List<Words> result =
                (List<Words>)getResultList.invoke(messageService, new String[]{"哈", "哈"}, new HashMap<String, Integer>(), new ArrayList<Words>());
        assertNotNull(result);
        assertEquals("哈",result.get(0).getName());
        assertEquals(2, result.get(0).getCount());
    }

}