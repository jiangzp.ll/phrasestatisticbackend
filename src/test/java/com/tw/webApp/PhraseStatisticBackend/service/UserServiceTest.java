package com.tw.webApp.PhraseStatisticBackend.service;

import com.tw.webApp.PhraseStatisticBackend.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import reactor.test.StepVerifier;

@SpringBootTest
class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    protected CouchbaseTemplate couchbaseTemplate;

    private static final String USER_ID = "user-1";

    @Test
    void should_saveUser() {

        User user = User.builder().id(USER_ID).name("tom").password("pass").build();

        StepVerifier.create(userService.saveUser(user))
                .expectNextMatches(u -> u.getId().equals(USER_ID))
                .verifyComplete();
    }

    @Test
    void should_getUserById() {

        User user = User.builder().id(USER_ID).name("tom").password("pass").build();
        prepareUser(user);

        StepVerifier.create(userService.getUserById(USER_ID))
                .expectNextMatches(u -> u.getId().equals(USER_ID) && u.getName().equals("tom"))
                .verifyComplete();
    }

    @Test
    void should_getAllUsers() {

        User user = User.builder().id(USER_ID).name("tom").password("pass").build();
        User otherUser = User.builder().id("user-2").name("bob").password("password").build();
        prepareUser(user);
        prepareUser(otherUser);

        StepVerifier.create(userService.getAllUsers())
                .expectNextCount(2)
                .verifyComplete();
    }

    @Test
    void should_updateUser() {

        User user = User.builder().id(USER_ID).name("tom").password("pass").build();
        User newUser = User.builder().name("bob").password("password").build();
        prepareUser(user);

        StepVerifier.create(userService.updateUser(USER_ID, newUser))
                .expectNextMatches(u -> u.getId().equals(USER_ID) && u.getName().equals("bob")
                        && u.getPassword().equals("password"))
                .verifyComplete();
    }

    @Test
    void should_deleteUserById() {

        User user = User.builder().id(USER_ID).name("tom").password("pass").build();
        User otherUser = User.builder().id("user-2").name("bob").password("password").build();
        prepareUser(user);
        prepareUser(otherUser);

        StepVerifier.create(userService.getAllUsers())
                .expectNextCount(2)
                .verifyComplete();

        StepVerifier.create(userService.deleteUserById(USER_ID))
                .expectComplete();

        StepVerifier.create(userService.getAllUsers())
                .expectNextCount(1)
                .expectNextMatches(u -> u.getName().equals("bob") && u.getPassword().equals("password"))
                .verifyComplete();
    }



    private void prepareUser(User user) {
        couchbaseTemplate.save(user);
    }
}